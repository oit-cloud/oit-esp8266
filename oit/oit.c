#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
#include <gpio.h>
#include <mem.h>
#include <espconn.h>
#include "oit.h"
#include "pwm.h"

static struct espconn *tcpconn;

// set in oit_init
static OitHandler *oitHandlers;

void ICACHE_FLASH_ATTR oit_tcp_connect_cb(void *arg) {
  os_printf("TCP connection.\n");
}

void ICACHE_FLASH_ATTR oit_tcp_recv_cb(void *arg, char *pdata, unsigned short len) {
  int i = 0;
  char dbg[64];
  char reply[128];
  Msg *msg = (Msg *)pdata;
  struct espconn *conn = (struct espconn *) arg;

  os_memset(&reply, 0, 128);

  OitHandler handler = oitHandlers[0];

  os_sprintf(&dbg, "msg->seq: %d\nmsg->cmd: %d\nmsg->index: %d\nmsg->value: %d\n", msg->seq, msg->cmd, msg->index, msg->value);
  os_printf(dbg);

  reply[0] = msg->seq;
  reply[1] = msg->cmd;
  
  switch(msg->cmd) {
    case 0x00:
      while(1) {
        reply[(i * 3) + 2] = i;
        reply[(i * 3) + 3] = oitHandlers[i].type;
        reply[(i * 3) + 4] = oitHandlers[i].getValCB(oitHandlers[i].args);

        os_printf("index: %d\ntype: %d\nvalue: %d\n", i, oitHandlers[i].type, reply[(i * 3) + 4]);

        if(oitHandlers[++i].type == NULL) break;
      }
    break;

    case 0x01:;
      if(oitHandlers[msg->index].setValCB != NULL) {
        oitHandlers[msg->index].setValCB((int)oitHandlers[msg->index].args, (int)msg->value);

        reply[3] = 1;
      }

      reply[3] = 0;

    break;
  }

  espconn_send(conn, reply, 32);
}

bool ICACHE_FLASH_ATTR tcp_server_init() {
  int ret;

  tcpconn = (struct espconn*)os_zalloc(sizeof(struct espconn));
  os_printf("Configuring TCP Server.\n");

  if(tcpconn) {
    tcpconn->type = ESPCONN_TCP;
    tcpconn->state = ESPCONN_NONE;

    os_printf("Setting TCP port.\n");
    tcpconn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));

    tcpconn->proto.tcp->local_port = 8448;

    os_printf("Registering TCP callback.\n\n");

    espconn_regist_connectcb(tcpconn, oit_tcp_connect_cb);
    espconn_regist_recvcb(tcpconn, oit_tcp_recv_cb);

    ret = espconn_accept(tcpconn);

    espconn_regist_time(tcpconn, 7200, 0);

    if(ret != ESPCONN_OK) {
      os_printf("Error setting up TCP connection listener.\n");

      return false;
    };

    return true;
  }

  os_printf("Cannot allocate for TCP connection listener.\n");

  return false;
}


void ICACHE_FLASH_ATTR oit_mdns_init() {
  char msg[128];
  struct ip_info sta_ip;
  struct mdns_info *info = (struct mdns_info *)os_zalloc(sizeof(struct mdns_info));

  wifi_get_ip_info(STATION_IF, &sta_ip);

  os_sprintf(msg, "OITOITOIT mDNS\nIP: " IPSTR "\nHostname: %s\n\n", IP2STR(&sta_ip.ip.addr), hostname);

  info->host_name = hostname;
  info->ipAddr = sta_ip.ip.addr; //ESP8266 station IP
  info->server_name = "oitdevice";
  info->server_port = 8448;
  os_memset(&info->txt_data, 0, sizeof(info->txt_data));;

  espconn_mdns_init(info);
}

void oit_wifi_handle_event_cb(System_Event_t *evt) {
  switch(evt->event) {
    case EVENT_STAMODE_GOT_IP:
      oit_mdns_init();
      tcp_server_init();
    break;
  }
}

uint32 gpioState = 0;

int ICACHE_FLASH_ATTR oitGpioToggleGetCB(int gpio) {
  int value = (gpioState >> gpio) & 1;

  os_printf("gpioState: %4x\ngpio: %d\nvalue: %d\n", gpioState, gpio, value);

  return value;
}

void ICACHE_FLASH_ATTR oitGpioToggleSetCB(int gpio, int value) {
  GPIO_OUTPUT_SET(gpio, value);

  char dbg[64];
  os_sprintf(&dbg, "gpio: %d\nvalue: %d\n", gpio, value);
  os_printf(dbg);

  if(value) {
    gpioState |= 1 << gpio;
  } else {
    gpioState &= ~(1 << gpio);
  }

  os_printf("gpioState: %4x\n", gpioState);
}

int ICACHE_FLASH_ATTR oitPwmGetCB(int gpio) {
  return 0;
}

static uint32 io_info[][3] = {
  {PWM_0_OUT_IO_MUX, PWM_0_OUT_IO_FUNC, PWM_0_OUT_IO_NUM},
  {PWM_1_OUT_IO_MUX, PWM_1_OUT_IO_FUNC, PWM_1_OUT_IO_NUM},
  {PWM_2_OUT_IO_MUX, PWM_2_OUT_IO_FUNC, PWM_2_OUT_IO_NUM}
};

void ICACHE_FLASH_ATTR oitPwmSetCB(int gpio, int value) {
  int channel;

  switch(gpio) {
    case 12:
      channel = 1;
      break;
    case 13:
      channel = 3;
      break;
    case 15:
      channel = 2;
      break;
    default:
      channel = 0;
  }

  if(channel == 0) {
    os_printf("Invalid PWM gpio provided. Use 12, 13 or 15.");
    return;
  }

  uint32 pwm_duty_init = 4294967295U / 2;

  os_printf("calculatin freq\n");
  uint32 freq = (value / 0xff) * 4294967295U;
  os_printf(" oitPwmSetCB\n");
  pwm_init(value, &pwm_duty_init, channel, io_info);
  pwm_start();

  pwm_set_period(freq);
  pwm_set_duty(4294967295U / 2, 0);
  os_printf("pwm init run\n");
}

void ICACHE_FLASH_ATTR oit_init(OitHandler *handlers) {
  char msg[256];
  oitHandlers = handlers;

  hostname = (char *)os_zalloc(sizeof(uint32) + 5);
  os_sprintf(hostname, "oit%06x", spi_flash_get_id());

  wifi_set_event_handler_cb(oit_wifi_handle_event_cb); 
  os_sprintf(msg, "oitHandlers: 0x%8x\n", oitHandlers[0].getValCB);
  os_printf(msg);
}
