oit-esp8266
===========
Firmware for ESP8266 to comminutate with Oit Cloud services.

Overview
--------
oit-esp8266 is the ESP8266 library for connecting to Oit Cloud.
In it's most basic form it provides the mDNS service for device
discovery along with a server to query the device capabilities
and interact with those services.

oit-esp8266 by default provies two device types. GPIO and PWM.
The GPIO is a basic on/off and the PWM can be anything from 0
to 4294967295 (uint32). GPIO can be used on most pins while
PWM is limited to pins 12, 13 and 15.

With the way oit-esp8266 is written it is possible to write
custom device types. Everything is based around callbacks so
creating you own custom type is as simple as providing `setValue`
and `getValue` callbacks.

Usage
-----
General usage will involve defining your devices and calling
`oit_init`, most likely from your `user_main`. It will handle
setting up mDNS and it's server once a wifi connection is made.
Device definition looks like the following.

```
OitHandler oitHandlers[] = {
  {OIT_TYPE_GPIO, oitGpioToggleGetCB, oitGpioToggleSetCB, 11, NULL },
  {OIT_TYPE_PWM, oitPwmGetCB, oitPwmSetCB, 12, __INT_MAX__ / 2 },
  { NULL }
};
```
Next just call `oit_init(oitHandlers)`.

Breaking down the definition there are 5 parts. `Type`, `getValueCB`, `setValueCB`, `GPIO` and `Initial Value`.
In this example, the callbacks are provided in the API so all that is needed is setting the pin
and initial value.
