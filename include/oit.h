#ifndef OIT_H
#define OIT_H
#define OIT_TYPE_GPIO 0x01
#define OIT_TYPE_PWM 0x02

#define PWM_0_OUT_IO_MUX PERIPHS_IO_MUX_MTDI_U
#define PWM_0_OUT_IO_NUM 12
#define PWM_0_OUT_IO_FUNC  FUNC_GPIO12

#define PWM_1_OUT_IO_MUX PERIPHS_IO_MUX_MTDO_U
#define PWM_1_OUT_IO_NUM 15
#define PWM_1_OUT_IO_FUNC FUNC_GPIO15

#define PWM_2_OUT_IO_MUX PERIPHS_IO_MUX_MTCK_U
#define PWM_2_OUT_IO_NUM 13
#define PWM_2_OUT_IO_FUNC FUNC_GPIO13

char *hostname;

typedef struct {
  int gpio;
  int value;
} OitHandlerArgs;

typedef int (* oitGetValueCB)(int gpio);
typedef void (* oitSetValueCB)(int gpio, int value);

typedef struct {
  int type;
  oitGetValueCB getValCB;
  oitSetValueCB setValCB;
  int args;
  int args2;
} OitHandler;

typedef struct {
  uint8 seq;
  uint8 cmd;
  uint8 index;
  uint8 value;
} Msg;

int oitGpioToggleGetCB(int gpio);
void oitGpioToggleSetCB(int gpio, int value);

int oitPwmGetCB(int gpio);
void oitPwmSetCB(int gpio, int value);
#endif
